// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import CMDataTable from '../pages/cm/dataTable';

describe('integration', () => {
    it('Collapse Show/Hide Columns', () => {
        CMDataTable.goToDataTable();
        cy.wait(1000);
        CMDataTable.getShowHideColumnsItem().eq(0).click().should('have.class', 'is-active');
    });

    it('Switch Column with label cost', () => {
        CMDataTable.goToDataTable();
        cy.wait(1000);
        CMDataTable.getShowHideColumnsItem().eq(0).click().should('have.class', 'is-active');
        cy.wait(1000);
        CMDataTable.getSwitchShowHideColumn().eq(0).click()
        CMDataTable.getLabelColumn().eq(0).contains('Cost').should('not.be.visible')
    });

    it('Filter table by Id and Reset Filters', () => {
        CMDataTable.goToDataTable();
        cy.wait(1000);
        CMDataTable.getInputFilterItems().eq(0).type(252);
        CMDataTable.clickSubmitFilters();
        cy.wait(1000);
        CMDataTable.getElTableRow().eq(0).find('td').contains('252');
        CMDataTable.getElTableRow().should('have.length', 1);
        CMDataTable.getInputFilterItems().eq(0).should('have.value', '252');
    });

    it('Page Limit items', () => {
        CMDataTable.goToDataTable();
        cy.wait(1000);
        CMDataTable.getElTableRow().should('have.length', 10);
        CMDataTable.clickSelectPageLimit();
        cy.get('.el-select-dropdown__list .el-select-dropdown__item').eq(1).click();
        cy.wait(1000);
        CMDataTable.getElTableRow().should('have.length', 20);
    });

    it('Pagination', () => {
        CMDataTable.goToDataTable();
        cy.wait(1000);
        CMDataTable.getPagination().find('.number').contains('1').should('have.class', 'active');
        CMDataTable.getPagination().find('.number').contains('2').click().should('have.class', 'active');
    });

    it('View single catalog', () => {
        CMDataTable.goToDataTable();
        cy.wait(1000);
        CMDataTable.clickBtnSingleItem();
        cy.url().should('include', 'view')
    });

    it('Sort', () => {
        CMDataTable.goToDataTable();
        cy.wait(1000);
        CMDataTable.getTheadTable().should('not.have.class', 'ascending');
        CMDataTable.getTheadTable().should('have.class', 'is-sortable');
        CMDataTable.getTheadTable().find('.ascending').eq(0).click({ multiple: true,force: true });
        CMDataTable.getTheadTable().should('have.class', 'ascending');
    });
});
