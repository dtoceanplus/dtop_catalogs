// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
const goToDataTable = () => {
    cy.visit('/catalog/collection_point');
};

const getShowHideColumnsItem = () => {
    return cy.get('[data-cy-cm="collapsItem"]');
};

const getSwitchShowHideColumn = () => {
    return cy.get('[data-cy-cm="switchShowHideColumn"]');
};

const getLabelColumn = () => {
    return cy.get('[data-cy-cm="labelColumn"]');
};

const getInputFilterItems = () => {
    return cy.get('[data-cy-cm="inputFilterItems"]');
};

const clickSubmitFilters = () => {
    return cy.get('[data-cy-cm="submitFilters"]').click({ multiple: true,force: true });
};

const clickResetFilters = () => {
    return cy.get('[data-cy-cm="resetFilters"]').click({ multiple: true,force: true });
};

const clickSelectPageLimit = () => {
    return cy.get('[data-cy-cm="selectPageLimit"]').click({ multiple: true,force: true });
};

const getPagination = () => {
    return cy.get('[data-cy-cm="pagination"]');
};

const getElTableRow = () => {
    return cy.get('.el-table__body-wrapper .el-table__body .el-table__row')
};

const clickBtnSingleItem = () => {
    return cy.get('[data-cy-cm="btnSingleItem"]').eq(0).click({ multiple: true,force: true });
};

const getTheadTable = () => {
    return cy.get('.el-table__header th')
}

export default {
    goToDataTable,
    getShowHideColumnsItem,
    getSwitchShowHideColumn,
    getLabelColumn,
    getInputFilterItems,
    clickSubmitFilters,
    clickResetFilters,
    clickSelectPageLimit,
    getPagination,
    getElTableRow,
    clickBtnSingleItem,
    getTheadTable
};
