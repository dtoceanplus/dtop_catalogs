FROM python:3.7-slim-buster

RUN apt-get update && \
    apt-get install postgresql postgresql-client libpq-dev -y && \
    apt-get install systemd -y

COPY . /app
WORKDIR /app

RUN apt-get update && \
    apt-get install --yes --no-install-recommends gcc libc6-dev && \
    pip install --requirement prod-requirements.txt && \
#    python setup.py install && \
    pip install --editable . && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes gcc libc6-dev

WORKDIR /app/src/backend/app/app

RUN chmod a+x *.sh

CMD bash
