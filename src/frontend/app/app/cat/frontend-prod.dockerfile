FROM node:14.15 as build-stage
# FROM node as build-stage

WORKDIR /app
#COPY ./gui .

# for build context  . :
COPY ./src/frontend/app/app/cat .

ARG DTOP_MODULE_SHORT_NAME
ARG DTOP_BASIC_AUTH

ENV VUE_APP_DTOP_MODULE_SHORT_NAME=$DTOP_MODULE_SHORT_NAME
ENV VUE_APP_DTOP_BASIC_AUTH=$DTOP_BASIC_AUTH

#RUN rm -rf node_modules/

#RUN pwd
#RUN ls -lart

#RUN npm install

RUN npm ci

RUN npm run build

FROM nginx:1.19-alpine

COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/prod.conf /etc/nginx/conf.d/local.conf

# for the build context -  .
#COPY ./gui/nginx.conf /etc/nginx/nginx.conf
COPY ./src/frontend/app/app/cat/nginx.conf /etc/nginx/nginx.conf
#COPY ./service/static/openapi.json /usr/share/nginx/html/static/openapi.json
COPY ./src/backend/app/app/api/dtop-catalog/0.0.1/openapi.json /usr/share/nginx/html/static/openapi.json

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
