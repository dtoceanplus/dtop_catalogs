# Some details
Complete all actions in <dtop_catalogs>/README.md

All input Environment variables are defined in the file :

<dtop_catalogs>/src/backend/backend.env

Prerequisites (development) :
1. [Node.js](https://nodejs.org/en/)

# Installation

```bash
cd <${FRONTEND}>

## Project setup
```
npm install
```
# Usage



npm run serve
```
In the browser open web UI of the Catalog API using URL :
http://localhost:8080/

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Tests and Code coverage
```
npm test
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# step by step acceptance scenario
- Go to the /catalog page, where the data table is located
- Go through three possible steps (view, edit, delete)
