// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  snapshotSerializers: [],
  collectCoverage: true,
  collectCoverageFrom: [
    "./src/components/CatalogBuild.vue",
    "./src/components/CatalogSingleBuild.vue",
    "./src/components/Test.vue",
    "./src/App.vue"
  ],
  coverageReporters: [
    "html",
    "text-summary",
    "clover"
  ],
  reporters: ["default", "<rootDir>/custom-reporter.js"],
  coverageThreshold: {
    global: {
      "statements": 52.29,
      "branches": 5.56,
      "functions": 52.78,
      "lines": 52.78
    }
  },
  moduleFileExtensions: [
    "js",
    "vue",
    "json"
  ],
  transform: {
    "^.+\\.js$": "<rootDir>/node_modules/babel-jest",
    ".*\\.(vue)$": "<rootDir>/node_modules/vue-jest"
  },
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1"
  }
}
