// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import { mount } from '@vue/test-utils'
import Test from '@/components/Test.vue'
jest.mock('axios')

describe('Test.vue', function() {
    var wrapper = mount(Test);
    it('pressing a button should increase the counter', function() {
        expect(wrapper.vm.count).toBe(0);
        var button = wrapper.find('.btnIncrement');
        button.trigger('click');
        expect(wrapper.vm.count).toBe(1);
    });

    it('makes an async request after click  button', done => {
        const wrapper = mount(Test)
        wrapper.find('.btnFetch').trigger('click')
        wrapper.vm.$nextTick(() => {
            expect(wrapper.vm.value).toBe('value')
            done()
        })
    })
});
