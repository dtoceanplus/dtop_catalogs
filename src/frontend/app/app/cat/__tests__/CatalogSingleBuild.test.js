// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import { mount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import jsonCatalog from '../json/jsonCatalog.json'
import jsonCard from '../json/jsonCard.json'
import CatalogSingleBuild from '@/components/CatalogSingleBuild.vue'
import ElementUI from 'element-ui';
import Vue from 'vue'
Vue.use(ElementUI);
jest.mock("axios");

describe('CatalogSingleBuild.vue', function() {
    const localVue = createLocalVue()
    let wrapper = mount(CatalogSingleBuild, {
        localVue,
        mocks: {
            $route: {
                params: {
                    id: 25,
                    actions: "edit"
                }
            }
        }
    })
    it("Get json data", async() => {
        const result = await wrapper.vm.getCardData();
        expect(result).toEqual(undefined);
    });
    it("Submit Form", () => {
        wrapper.vm.prepareCard(jsonCard);
        expect(wrapper.vm.cardData).toEqual(jsonCard.data.attributes);
        expect(wrapper.vm.type).toEqual(jsonCard.data.type);
    });
    it("Save update", async() => {
        const result = await wrapper.vm.submitForm();
        expect(result).toEqual(wrapper.vm.cardData.id);
    });
});
