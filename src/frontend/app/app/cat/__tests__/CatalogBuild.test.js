// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import { mount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import axios from 'axios'

import CatalogBuild from '@/components/CatalogBuild.vue'
import jsonCatalog from '../json/jsonCatalog.json'
import jsonCatalogTableData from '../json/jsonCatalogTableData.json'
import jsonCatalogTableHeader from '../json/jsonCatalogTableHeader.json'

import ElementUI from 'element-ui';
import Vue from 'vue'

Vue.use(ElementUI);
jest.mock("axios");

describe('CatalogBuild.vue', function() {
    const localVue = createLocalVue()
    localVue.use(VueRouter)
    const routes = [{
        path: '/catalog/:id/:action',
    }];
    const router = new VueRouter({ routes })
    var wrapper = mount(CatalogBuild, {
        localVue,
        router
    });
    // test("Delete row table btn", async() => {
    //     axios.resolveWith(jsonCatalog)
    //     wrapper.vm.tableData = jsonCatalogTableData;
    //     await wrapper.vm.$nextTick()
    //     wrapper.find('.btn-delete').trigger('click')
    //     await wrapper.vm.$nextTick()
    //     wrapper.vm.$router.push('/catalog')
    //     wrapper.vm.tableData = ""
    // });
    // test("Redirect catalog Delete page", async() => {
    //     wrapper.vm.tableData = jsonCatalogTableData;
    //     wrapper.vm.tableHeader = jsonCatalogTableHeader;
    //     await wrapper.vm.$nextTick()
    //     wrapper.find('.btn-edit').trigger('click')
    //     const idRow = wrapper.vm.tableData[0].id
    //     expect(wrapper.vm.$route.path).toBe('/catalog/' + idRow + '/edit')
    // });

    // test("Redirect catalog Single page", async() => {
    //     wrapper.vm.$router.push('/catalog')
    //     await wrapper.vm.$nextTick()
    //     const result = await wrapper.vm.getTableData();
    //     expect(result).toEqual(jsonCatalog);
    // });
    // test("Prepare table", () => {
    //     wrapper.vm.prepareTable(jsonCatalog);
    //     expect(wrapper.vm.tableData).toEqual(jsonCatalogTableData);
    //     expect(wrapper.vm.tableHeader).toEqual(jsonCatalogTableHeader);
    // });
    // test("Delete row table func", () => {
    //     const idRow = jsonCatalogTableData[0].id
    //     const jsonCatalogTableDataLength = jsonCatalogTableData.length
    //     wrapper.vm.tableData = jsonCatalogTableData;
    //     wrapper.vm.deleteRowIndex(0);
    //     expect(wrapper.vm.tableData.length).toEqual(jsonCatalogTableDataLength - 1);
    // });
    // test("Select element", () => {
    //     wrapper.find('.radio').trigger('click')
    //     expect(wrapper.vm.radio).toEqual(22);
    // });

    // test("Select element handler when item not selected", () => {
    //     wrapper.vm.radio = null
    //     expect(wrapper.vm.select()).toEqual(false);
    // });

    test("Get json data", async() => {
        axios.resolveWith(jsonCatalog)
        const result = await wrapper.vm.getTableData();
        expect(result).toEqual(jsonCatalog);
    });
    // test("Select element handler when library is not availible", () => {
    //     wrapper.find('.radio').trigger('click')
    //     global.console = {
    //       log: jest.fn()
    //     }
    //     wrapper.vm.select()
    //     expect(global.console.log).toHaveBeenCalledWith('Integration library is not availible')
    // });

    // test("Select element handler when all is ok", () => {
    //     global.dtop = {
    //       eventManager: {
    //         emit: jest.fn()
    //       }
    //     }
    //     wrapper.vm.select()
    //     expect(global.dtop.eventManager.emit).toHaveBeenNthCalledWith(1, "selected", {"id": 22})
    //     expect(global.dtop.eventManager.emit).toHaveBeenNthCalledWith(2, "done")
    // });

    // test("Get json data", async() => {
    //     let res = '{"data":[{"a":{"b":123,"c":345,"d":"test"}}]}'
    //     axios.resolveWith(res)
    //     const result = await wrapper.vm.getCardData();
    //     expect(result).toEqual(res);
    // });

});
