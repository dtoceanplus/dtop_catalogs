// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
export default {
    get() {
        return Promise.resolve(this.__result)
    },
    delete() {
        return Promise.resolve(this.__result)
    },
    patch() {
        return Promise.resolve(this.__result)
    },
    post() {
        return Promise.resolve(this.__result)
    },
    put() {
        return Promise.resolve(this.__result)
    },
    resolveWith(data) {
        this.__result = data
    }
}
