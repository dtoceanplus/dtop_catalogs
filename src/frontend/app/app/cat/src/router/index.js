// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from 'vue'
import VueRouter from 'vue-router'

import Catalog from '../views/Catalog.vue';
import CatalogSingle from '../views/CatalogSingle.vue';
import HomePage from '../views/HomePage.vue';

Vue.use(VueRouter);
const routes = [{
    path: '/catalog/:data',
    name: 'Catalog',
    component: Catalog
},
{
    path: '/catalog/:data/:id/:action',
    name: 'CatalogSingle',
    component: CatalogSingle
},
{
    path: '/',
    name: 'HomePage',
    component: HomePage
},
{
    path: "*",
    name: "404",
    component: () =>
        import(/* webpackChunkName: "profile" */ "../views/404.vue")
},
];
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
