// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'

import router from './router/';
import ApiService from "./common/api.service";

import 'element-ui/lib/theme-chalk/index.css';
import "@/assets/css/style.scss"

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });

ApiService.init();

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
