// This is the Catalogue Module for the DTOceanPlus suite of Tools.
// This module manages common Catalogues for DTOceanPlus modules.
// Copyright (C) 2021 OPEN CASCADE
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
/* eslint-disable */
import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

const ApiService = {
  init() {
	Vue.use(VueAxios, axios);
  },

  query(resource, params) {
	return Vue.axios.get(resource, params).catch(error => {
	  throw new Error(`ApiService ${error}`);
	});
  },

  get(resource,filter) {
	return Vue.axios.get(`${resource}`,
	  {
		params: {
		  filter: filter
		}
	  }
	).catch(error => {
	  throw new Error(`ApiService ${error}`);
	});
  },

  post(resource, params) {
	return Vue.axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
	return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
	return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
	return Vue.axios.delete(resource).catch(error => {
	  throw new Error(`ApiService ${error}`);
	});
  }
};

export default ApiService;
