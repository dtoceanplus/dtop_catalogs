#/bin/bash

# Description:
#  The script makes a initial dump of CM database
#  in running docker stack service container
#  for using in catalog stack of installation

#
# Run Information:
#  cd src/backend/app/app
#  chmod u+x ./dump_db.sh
#  ./dump_db.sh

set -e

#docker exec $(docker ps -q -f name=cm_backend) bash -c "PGPASSWORD=postgres pg_dump --username=postgres --host=localhost --clean dtop-catalog" > ./cm_db_dump$(date +_%Y_%m_%d_%H_%M_%S)_sql
docker exec $(docker ps -q -f name=cm-backend) bash -c "PGPASSWORD=postgres pg_dump --username=postgres --host=localhost --clean dtop-catalog" > ./data/cm_postgres_1_db_reinitialize_sql
