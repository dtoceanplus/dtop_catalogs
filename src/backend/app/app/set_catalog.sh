#!/bin/bash

# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

usage () {
    cat <<HELP_USAGE

    The script sets the current dtop catalog variables based on <Excel file name> to the <environment file>

    Usage :   $0 <Excel file name> <environment file>

    example : $0 dtop-catalog-electrical_network_equipment.xlsx ../../backend.env

HELP_USAGE
}

if [ $# -ne 2 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi

cur_catalog="$1"
cat_base_name="${cur_catalog%.*}"

if [ ! -f data/$cur_catalog ]; then
    echo "$cur_catalog Excel file not found"
    exit 1
fi

backenv_file="$2"

cat ${backenv_file}

export $(grep -v "^#" $backenv_file | xargs)

sed -i "s/$POSTGRESQL_DATABASE/$cat_base_name/g" $backenv_file

echo "----------------------------------"
echo "The following catalog has been set: "
echo " ----------------------------------"
cat ${backenv_file}
echo "----------------------------------"
