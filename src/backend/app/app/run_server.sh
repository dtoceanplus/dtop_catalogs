#!/bin/bash

# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

usage () {
    cat <<HELP_USAGE

    The script performs preparation and population of database from Excel
    and running Flask service that exposes OpenAPI/Swagger-generated API

    Usage :   $0

HELP_USAGE
}

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi

if [ ! -f ../../backend.env ]; then
    echo "../../backend.env is not found"
    exit 1
fi

export $(grep -v "^#" ../../backend.env | xargs)

service postgresql start

service postgresql status

su -c "psql -c \"ALTER USER postgres PASSWORD '$POSTGRESQL_PASSWORD' \"" postgres

python core/dbutils.py create_db

python core/dbutils.py check_db

python core/checktypes.py

python core/excel2db.py

python core/runapi.py
