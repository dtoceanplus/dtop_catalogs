# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module db2excel
'''''''
The module exports catalog data from postgres database to Excel file

.. automodule:: core.db2excel
"""

import os
import pandas as pd
import sqlalchemy as sa
import psycopg2

from datetime import datetime

import appenv
from appenv import geturl

filedir = os.path.dirname(os.path.realpath(__file__))
datadir = os.path.join(filedir, '../data/')

cur_time = datetime.now().strftime("_%Y_%m_%d_%H_%M_%S")
xls_export_file = datadir + os.environ['CATALOG_EXCEL_EXPORT_FILE'] + cur_time + ".xlsx"

connection_url = geturl()
engine = sa.create_engine(connection_url,echo=False)

table_list_sql="SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';"
try:
    df_tables_list = pd.read_sql(table_list_sql, connection_url)
except Exception as e:
    print('>>>> read from database issue : ' + str(e))
    exit(1)

# writer = pd.ExcelWriter((datadir + os.environ['CATALOG_EXCEL_EXPORT_FILE'] + cur_time + ".xlsx"))
writer = pd.ExcelWriter(xls_export_file)

for index, row in df_tables_list.iterrows():
    print (row["table_name"])
    table_content_sql="SELECT * FROM " + row["table_name"] + ";"
    df_table = pd.read_sql(table_content_sql, con=connection_url)
    df_table.to_excel(writer, row["table_name"])

try :
    writer.save()
except (Exception, PermissionError) as e:
    print('>>>> write to excel file issue : ' + str(e))
    print(">>>> Probably the file is opened now - please close it and retry")
    exit(1)

head, tail = os.path.split(xls_export_file)

print (tail + " file is exported from database to data directory")
