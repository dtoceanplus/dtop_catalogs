# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module excel2db
'''''''
The module imports data from Excel file presenting dtop catalog to postgres database

.. automodule:: core.excel2db
"""

import os
import pandas as pd
import sqlalchemy as sa
import psycopg2
from sqlalchemy_utils import database_exists
import json
import appenv
from appenv import geturl

filedir = os.path.dirname(os.path.realpath(__file__))
datadir = os.path.join(filedir, '../data/')
xls_name = os.environ['CATALOG_EXCEL_IMPORT_FILE']
src_excel_file = datadir + xls_name

data_types_template_file_name = os.path.splitext(xls_name)[0] + ".json"
data_types_template_path = datadir + data_types_template_file_name

if os.path.isfile(data_types_template_path):
    print (data_types_template_file_name + " file exists")
else:
    print (data_types_template_file_name + " file doesn't exist.")
    exit()

try:
    xls = pd.ExcelFile(src_excel_file)
except Exception as e:
    print('>>>> open excel file issue : ' + str(e))
    exit(1)

connection_url = geturl()

engine = sa.create_engine(connection_url,echo=False)

if database_exists(engine.url) :
    print(os.environ['POSTGRESQL_DATABASE'] + " database exists")
else :
    print(os.environ['POSTGRESQL_DATABASE'] + " database doesn't exist")
    exit()

print('Excel sheet names Number = ' + str(len(xls.sheet_names)))
print('Excel sheet names : ')
print(xls.sheet_names)

excel_sheets_names = []
db_tables_names = []

for sheet_name in xls.sheet_names:
    excel_sheets_names.append(sheet_name)
    db_tables_names.append(sheet_name)

with engine.connect() as conn :
    i = 1
    for sheet_name in xls.sheet_names:
        print(f'\nsheet number = {i}')
        print("sheet_name = " + sheet_name)
        try:
            with open(data_types_template_path, 'r') as f:
              data_types = json.load(f)
            # print(data_types)
            print ("Datatypes dtype for sheet = " + sheet_name)
            sheet_dtype = data_types[sheet_name]
            print (sheet_dtype)

            df = pd.read_excel(xls, sheet_name=sheet_name, dtype=sheet_dtype)

            print("Number of the columns = " + str(len(df.columns)))

        except Exception as er:
            print('>>>> read from excel file issue : ' + str(er))
            exit(1)
        try:
            # print("max id :")
            # print(df['id'].max())
            df.reset_index(drop=True, inplace=True)

            sql_db = pd.io.sql.SQLDatabase(engine)
            sql_db.drop_table(sheet_name)

            if not sql_db.has_table(sheet_name):
                args = [sheet_name, sql_db]
                kwargs = {
                    "frame" : df,
                    "index" : True,
                    "index_label" : "id",
                    "keys" : "id"
                }
                sql_table = pd.io.sql.SQLTable(*args, **kwargs)
                sql_table.create()

            df.to_sql(name=sheet_name, con=engine, if_exists='append', index=False)
        except Exception as e:
            print('>>>> write to database issue : ' + str(e))
            exit(1)

        i += 1

conn.close()

print("set max value for primary key and sequence :")

print("Database Table's names :")
tables_list = engine.table_names()
print(tables_list)

def set_max_value_for_primary_key_sequence(table_name):
    table_key_seq = table_name + "_id_seq"
    print(table_key_seq + " max value : ")
    res = engine.execute("SELECT setval('" + table_key_seq + "' , max(id)) FROM " + table_name + ";")
    for row in res:
        print(row)

for tbl in tables_list:
    set_max_value_for_primary_key_sequence(tbl)

print ("Data from " + os.environ['CATALOG_EXCEL_IMPORT_FILE'] + " file are exported to the database")
