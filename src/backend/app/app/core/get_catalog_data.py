# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import requests
from requests.exceptions import ConnectionError
import json
import sys
import argparse

# endpoint='http://localhost:7777/api/mooring_line'
# endpoint='http://localhost:7777/api/mooring_line/2'

def get_data(endpoint=None) :

    try:
        resp = requests.get(endpoint)
        resp.raise_for_status()
    except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:", errc)
        print(" ! probably the module server is not available")
        exit(1)
    except requests.exceptions.HTTPError as errh:
        print ("Http Error:", errh)
        exit(1)
    except requests.exceptions.RequestException as err:
        print ("Some Request Error", err)
        exit(1)

    print(resp)

    catalog_data = resp.json()['data']

    if type(catalog_data) is list:
        print("response is processed as a list")
        catalog_data_attributes_list=[]
        catalog_data_attributes_dict={}
        for x in range(len(catalog_data)):
          catalog_data_attributes_dict = catalog_data[x]['attributes']
          catalog_data_attributes_list.append(catalog_data_attributes_dict)
        print (json.dumps(catalog_data_attributes_list, indent=1))
    elif type(catalog_data) is dict:
        print("response is proxessed as a dictionary")
        catalog_data = resp.json()['data']['attributes']
        print (json.dumps(catalog_data, indent=1))
    else :
        print("response is can not be processed either as a list or a dictionary")
        exit(0)


    # http://localhost:7777/api/mooring_line/?fields[MooringLine]=id,catalogue_id,type,material,quality
    # {"data":[{"attributes":{"catalogue_id":"chain_1","id":1,"material":"steel","quality":"R3","type":"chain"},"id":1,"links":{"self":"http://localhost:7777/api/mooring_line/1"},"relationships":{},"type":"MooringLine"},{"attributes":{"catalogue_id":"wire_rope_1","id":2,"material":"steel","quality":"none","type":"wire_rope"},"id":2,"links":{"self":"http://localhost:7777/api/mooring_line/2"},"relationships":{},"type":"MooringLine"}],"jsonapi":{"version":"1.0"},"links":{"self":"http://localhost:7777/api/mooring_line/?fields[MooringLine]=id,catalogue_id,type,material,quality&page[offset]=0&page[limit]=250"},"meta":{"count":2,"limit":250}}
    # "attributes" is list of dictionaries

    # http://localhost:7777/api/mooring_line/1?fields[MooringLine]=id,catalogue_id,type,material,quality
    # {"data":{"attributes":{"catalogue_id":"chain_1","id":1,"material":"steel","quality":"R3","type":"chain"},"id":1,"links":{"self":"http://localhost:7777/api/mooring_line/1"},"relationships":{},"type":"MooringLine"},"jsonapi":{"version":"1.0"},"links":{"related":"http://localhost:7777/api/mooring_line/1?fields%5BMooringLine%5D=id,catalogue_id,type,material,quality","self":"http://localhost:7777/api/mooring_line/1"},"meta":{"count":1,"instance_meta":{},"limit":250}}
    # "attributes" is dictionary



def main(command_arg=None):
    parser = argparse.ArgumentParser()

    parser.add_argument('--endpoint', help='endpoint of run catalog server', required=True)

    args = parser.parse_args()

    # print(f'Hello {args.endpoint}')

    get_data(args.endpoint)

    if len(sys.argv) <= 1:
        sys.argv.append('--help')

if __name__ == '__main__':
    main()
