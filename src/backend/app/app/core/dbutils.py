# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module dbutils
'''''''
The module manages creating and dropping dtop catalog database in postgres database

.. automodule:: core.dbutils
"""

import argparse
import os
import psycopg2

import sqlalchemy as sa
from sqlalchemy_utils import database_exists, create_database, drop_database
import sys

import appenv
from appenv import geturl

connection_url = geturl()
engine = sa.create_engine(connection_url,echo=False)

def check_db() :
    if database_exists(engine.url) :
        print(os.environ['POSTGRESQL_DATABASE'] + " database exists")
    else :
        print(os.environ['POSTGRESQL_DATABASE'] + " database doesn't exist")

def create_db():
    if database_exists(engine.url) :
        print(os.environ['POSTGRESQL_DATABASE'] + " database already exists")
    else :
        create_database(engine.url)
        print(os.environ['POSTGRESQL_DATABASE'] + " database is created")

def drop_db() :
    if database_exists(engine.url) :
        drop_database(engine.url)
        print(os.environ['POSTGRESQL_DATABASE'] + " database is dropped")
    else :
        print(os.environ['POSTGRESQL_DATABASE'] + " database doesn't exist")


def main(command_arg=None):

    parser = argparse.ArgumentParser()

    # subparsers = parser.add_subparsers()
    subparsers = parser.add_subparsers(dest='command')

    # Create a check_db subcommand
    parser_check_db = subparsers.add_parser('check_db', help='checks if dtop catalog database exists')

    # Create a create_db subcommand
    parser_create_db = subparsers.add_parser('create_db', help='creates dtop catalog database in postgres')

    # Create a drop_db subcommand
    parser_drop_db = subparsers.add_parser('drop_db', help='drops dtop catalog database in postgres')

    if len(sys.argv) <= 1:
        sys.argv.append('--help')

    options = parser.parse_args(command_arg)

    if options.command == 'check_db':
        check_db()
    elif options.command == 'create_db':
        create_db()
    elif options.command == 'drop_db':
        drop_db()

if __name__ == '__main__':
    main()
