# Example of DTOP BE modules interaction.
# Catalog BE calls Main Module BE to get the users' list
#

import requests
import os

# Common function.
# to get DTOP basic module URL and headers
# module - DTOP module nichname
def get_url_and_headers (module):

	protocol = os.getenv('DTOP_PROTOCOL', 'http')
	domain = os.environ['DTOP_DOMAIN']
	auth = os.getenv('DTOP_AUTH', '')

#"'Authorization': 'Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE='"
#	auth = "Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE="

	print()
	print("Authorization header value :")
	header_auth = f'{auth}'
	print(header_auth)

	print()
	print("Headers - auth :")
	headers = {}
	headers['Authorization'] = header_auth
	print(headers)

	print()
	print("Host header value :")
	header_host = f'{module + "." + domain}'
	print(header_host)

#	protocol="https"
#	domain="dto.opencascade.com"
#	server_url = f'{protocol + "://" + module + "." + domain}'
	server_url = f'{protocol + "://" + header_host}'

	print()
	print("Check if DTOP is deployed on DNS resolved server or on workstation :")

	try:
		response = requests.get(server_url, headers=headers)
		print('DTOP appears to be deployed on DNS resolved server')
		module_api_url = f'{server_url}'
	except requests.ConnectionError as exception:
		print('DTOP appears to be deployed on workstation')
		docker_ws_ip = "http://172.17.0.1:80"
		module_api_url = f'{docker_ws_ip}'
		print()
		print("Headers - auth and localhost :")
		headers['Host'] = header_host
		print(headers)

	print()
	print("Basic Module Open API URL :")
	print(module_api_url)
	print()

	return module_api_url, headers

# Example Test function.
# Catalog BE calls Main Module BE to get the users' list
def get_mm_users():

	print()
	print("Test example - request from Catalog BE to Main Module BE to get users' list")

	print()
	mm_api_url, headers = get_url_and_headers("mm")
	print("MM Open API URL to Get users :")
	mm_users_url = f'{mm_api_url + "/api/users"}'
	print(mm_users_url)

	print()
	print("CM BE to MM BE request and response :")
	print()

	try:
		resp = requests.get(mm_users_url, headers=headers)
		resp.raise_for_status()
	except requests.exceptions.ConnectionError as errc:
		print ("Error Connecting:", errc)
		print (" ! probably the module server is not available")
		exit(1)
	except requests.exceptions.HTTPError as errh:
		print ("Http Error:", errh)
		exit(1)
	except requests.exceptions.RequestException as err:
		print ("Some Request Error", err)
		exit(1)

	print(resp.json())
	print()

get_mm_users()
