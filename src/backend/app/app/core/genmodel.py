# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module genmodel
'''''''
The module generates SQLAlchemy ORM data model for catalog data from postgres database

.. automodule:: core.genmodel
"""
import os
import sys
import appenv

filedir = os.path.dirname(os.path.realpath(__file__))
safrsdir = os.path.join(filedir, '../safrs/')
sys.path.insert(0, safrsdir)

import expose_db.expose
from expose_db.expose import set_args, codegen

args=set_args()
# print(args)

models = codegen(args)

model_file_name = os.path.splitext(os.environ['CATALOG_EXCEL_IMPORT_FILE'])[0]+"_models.py"

target_dir = 'models/'

if not os.path.exists(target_dir):
  os.makedirs(target_dir)

with open(target_dir + model_file_name, 'w') as writer:
  writer.write(models)

print (model_file_name + " file is generated in models directory")
