# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module genapi
'''''''
The module creates OpenAPI/Swagger API in api/'api_version'/'xls_dir_name' backend directory

.. automodule:: core.genapi
"""
import os
import sys
import appenv
import shutil
import urllib.request, urllib.error
import json
import subprocess

filedir = os.path.dirname(os.path.realpath(__file__))
safrsdir = os.path.join(filedir, '../safrs/')
sys.path.insert(0, safrsdir)

import expose_db.expose

xls_name = os.environ['CATALOG_EXCEL_IMPORT_FILE']
xls_dir_name = os.path.splitext(xls_name)[0]
api_version = os.environ['API_VERSION']

source = 'api/' + xls_dir_name + "/" + api_version

if not os.path.exists(source):
    os.makedirs(source)

sw_url="http://" + os.environ['CATALOG_API_HOST_IP'] + ":" + os.environ['CATALOG_API_PORT'] + "/api/swagger.json"

try:
    conn = urllib.request.urlopen(sw_url)
except urllib.error.HTTPError as e:
    # Return code error (e.g. 404, 501, ...)
    print('HTTPError: {}'.format(e.code))
    exit(1)
except urllib.error.URLError as e:
    # Not an HTTP-specific error (e.g. connection refused)
    print('catalog API service is not run')
    print('URLError: {}'.format(e.reason))
    exit(1)
else:
    # 200
    with conn:
        print('catalog API service is run')
        data = json.loads(conn.read().decode())

sw_json = source+'/swagger.json'
sw_yaml = source+'/swagger.yaml'

api_json = source+'/openapi.json'
api_yaml = source+'/openapi.yaml'

with open(sw_json, 'w', encoding='utf-8') as outfile:
    json.dump(data, outfile, ensure_ascii=False, indent=2)
    print ("swagger.json file is generated")

sw_cli_cmd = ['swagger-cli', 'bundle', '-o', sw_yaml, '-t', 'yaml', sw_json]
try:
    if (subprocess.getstatusoutput('swagger-cli -h')[0] !=0):
        print("swagger-cli is not installed")
        exit(1)
    if os.name == 'nt':
        subprocess.run(sw_cli_cmd, capture_output=True, shell=True)
    else:
        subprocess.run(sw_cli_cmd, capture_output=True)
except subprocess.CalledProcessError as e:
    if (e.returncode != 0):
        exit(1)

print ("swagger.yaml file is generated")

json_conv_cmd = ['api-spec-converter', '--check', '--from=swagger_2', '--to=openapi_3', '--syntax=json', sw_json]
yaml_conv_cmd = ['api-spec-converter', '--check', '--from=swagger_2', '--to=openapi_3', '--syntax=yaml', sw_yaml]

try:
    if (subprocess.getstatusoutput('api-spec-converter -h')[0] !=0):
        print("api-spec-converter is not installed")
        exit(1)
    with open(api_json, 'w') as f:
        if os.name == 'nt':
            subprocess.run(json_conv_cmd, stdout=f, shell=True)
        else:
            subprocess.run(json_conv_cmd, stdout=f)
    with open(api_yaml, 'w') as f:
        if os.name == 'nt':
            subprocess.run(yaml_conv_cmd, stdout=f, shell=True)
        else:
            subprocess.run(yaml_conv_cmd, stdout=f)

except subprocess.CalledProcessError as e:
    if (e.returncode != 0):
        exit(1)


json_prettier_cmd = ['prettier', '--write', api_json]
yaml_prettier_cmd = ['prettier', '--write', api_yaml]

try:
    if (subprocess.getstatusoutput('prettier -h')[0] !=0):
        print("prettier is not installed")
        exit(1)
    if os.name == 'nt':
        subprocess.run(json_prettier_cmd, capture_output=True, shell=True)
        subprocess.run(yaml_prettier_cmd, capture_output=True, shell=True)
    else:
        subprocess.run(json_prettier_cmd, capture_output=True)
        subprocess.run(yaml_prettier_cmd, capture_output=True)
except subprocess.CalledProcessError as e:
    if (e.returncode != 0):
        exit(1)

print ("openapi.json and openapi.yaml files are created and prettified in " + source + " directory")
