# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module checktypes
'''''''
The module checks Data Types for source Excel file
.. automodule:: core.checktypes
"""

import os
import pandas as pd
import json

from pandas.io.json._table_schema import build_table_schema
from pandas.api.types import infer_dtype

import appenv
from appenv import geturl

filedir = os.path.dirname(os.path.realpath(__file__))
datadir = os.path.join(filedir, '../data/')

xls_name = os.environ['CATALOG_EXCEL_IMPORT_FILE']
src_excel_file = datadir + xls_name

data_types_template_file_name = os.path.splitext(xls_name)[0] + ".json"
data_types_template_path = datadir + data_types_template_file_name

def has_mixed_types(col):
    is_mixed = False
    if infer_dtype(col, skipna=True) in ['mixed', 'mixed-integer']:
        # print(" Data type in the column '" + col.name + "' are Mixed : " + infer_dtype(col, skipna=True))
        print("  Mixed Data types : '" + infer_dtype(col, skipna=True) + "' in the column = '" + col.name + "'")
        is_mixed = True
    return is_mixed

try:
    xls = pd.ExcelFile(src_excel_file)
except Exception as e:
    print('>>>> open excel file issue : ' + str(e))
    exit(1)

print('Excel sheet names Number = ' + str(len(xls.sheet_names)))
print('Excel sheet names : ')
print(xls.sheet_names)

excel_sheets_names = []

for sheet_name in xls.sheet_names:
    excel_sheets_names.append(sheet_name)



template_json={}
has_message = False

for sheet_name in xls.sheet_names:
    try:
        df = pd.read_excel(xls, sheet_name=sheet_name)
    except Exception as er:
        print('>>>> read from excel file issue : ' + str(e))
        exit(1)

    template_json[sheet_name] = build_table_schema(df)

    print("============================")
    print("The sheet = " + sheet_name)
    print("Number of the columns  = " + str(len(df.columns)))
    for column in df:
        if (has_mixed_types(df[column]) == True):
            has_message = True


with open(data_types_template_path, 'w') as f:
  json.dump(template_json, f, indent=2)
  f.write("\n")

print("============================")
print("JSON file defining current Data types that will be used is generated  : data/" + data_types_template_file_name)
if has_message :
    print("   Please review this JSON file and Mixed Data types messages above")
    print("   If necessary check and update the column Data in source Excel file :  data/" + xls_name)
