# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module copyapi
'''''''
The module copies/publishes the generated OpenAPI to api-cooperative/cats created as submodule.

.. automodule:: core.copyapi
"""
import os
import sys
import appenv
import shutil

xls_name = os.environ['CATALOG_EXCEL_IMPORT_FILE']
xls_dir_name = os.path.splitext(xls_name)[0]
api_version = os.environ['API_VERSION']

source = 'api/' + xls_dir_name + "/" + api_version
dest = '../../../../api/' + xls_dir_name + "/" + api_version

if not os.path.exists(dest):
    os.makedirs(dest)

# source_files = os.listdir(source)
# for file_name in source_files:
#     full_file_name = os.path.join(source, file_name)
#     if os.path.isfile(full_file_name) and os.path.splitext(file_name)[0] == 'openapi' :
#         shutil.copy(full_file_name, dest)

full_file_name = os.path.join(source, 'openapi.yaml')
shutil.copy(full_file_name, dest)

print('Done')
