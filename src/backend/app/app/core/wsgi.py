# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module wsgi
'''''''
Initialization of DTOP Catalog Backend Flask service for Gunicorn WSGI HTTP Server

.. automodule:: core.wsgi
"""

import os
import sys
import appenv

filedir = os.path.dirname(os.path.realpath(__file__))
safrsdir = os.path.join(filedir, '../safrs/')
sys.path.insert(0, safrsdir)

from expose_db.expose import *

app = create_app()

print ("DTOP Catalog backend service is started under Gunicorn WSGI HTTP Server ...")
