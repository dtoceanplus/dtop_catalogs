# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module checkapi
'''''''
The module checks the generated OpenAPI using the validators

.. automodule:: core.checkapi
"""
import os
import sys
import appenv
import subprocess
import pytest
import openapi_spec_validator
from openapi_spec_validator import validate_spec
from openapi_spec_validator import openapi_v2_spec_validator, openapi_v3_spec_validator
from jsonschema.exceptions import ValidationError
from yaml import safe_load
import json
import pathlib

xls_name = os.environ['CATALOG_EXCEL_IMPORT_FILE']
xls_dir_name = os.path.splitext(xls_name)[0]
api_version = os.environ['API_VERSION']
source = "api/" + xls_dir_name + "/" + api_version
test_consumer = os.environ['TEST_CONSUMER']

test_consumer_dir = "api/dtop-cat-consumer/" + api_version

test_consumer_yaml = test_consumer_dir + '/'+ test_consumer + '.yaml'
bundled_test_consumer_yaml = test_consumer_dir + '/bundled_'+ test_consumer + '.yaml'

api_json = source+'/openapi.json'
api_yaml = source+'/openapi.yaml'

for filename in [api_json, api_yaml, test_consumer_yaml]:
  if os.path.isfile(filename):
    print (filename + " exists")
  else:
    print (filename + " doesn't exist")
    exit()

def swagger_cli_validation (sw_cli_cmd):
    try:
        if (subprocess.getstatusoutput('swagger-cli -h')[0] !=0):
            print("swagger-cli is not installed")
            exit(1)
        if os.name == 'nt':
            res = subprocess.run(sw_cli_cmd, capture_output=True, shell=True)
        else:
            res = subprocess.run(sw_cli_cmd, capture_output=True)
        isvalid = False
        if (b"is valid" in res.stdout):
            isvalid = True
        return res.returncode, isvalid
    except subprocess.CalledProcessError as e:
        if (e.returncode != 0):
            exit(1)

def validate_with_swagger_cli(file_name) :
    sw_cli_cmd = ['swagger-cli', 'validate', file_name]
    suff = sw_cli_cmd[2]
    # print(suff)
    code, validity = swagger_cli_validation(sw_cli_cmd)
    if code == 0 and validity == True:
        print(suff + " is validated with swagger-cli")
    else:
        print(suff + " is not validated with swagger-cli")


validate_with_swagger_cli(api_json)
validate_with_swagger_cli(api_yaml)
validate_with_swagger_cli(test_consumer_yaml)

def validate_with_openapi_spec(api_file):
    suff = api_file.split('.')[-1]

    if suff == 'json' :
      with open(api_json) as f:
          spec = json.load(f)

    if suff == 'yaml' :
      with open(api_yaml) as f:
          spec = safe_load(f.read())

    # choose the validator
    validators = {
        '2.0': openapi_v2_spec_validator,
        '3.0.0': openapi_v3_spec_validator,
    }
    # validator = validators['3.0.0']
    validator = openapi_v3_spec_validator

    # validate
    try:
        validator.validate(spec, spec_url='')
    except ValidationError as exc:
        print(exc)
        sys.exit(1)
    except Exception as exc:
        print(exc)
        sys.exit(2)
    else:
        print(api_file.split('/')[-1], "is validated with openapi-spec-validator")

validate_with_openapi_spec(api_json)
validate_with_openapi_spec(api_yaml)

sw_cli_bundle_cmd = ['swagger-cli', 'bundle', '-o', bundled_test_consumer_yaml, '-t', 'yaml', test_consumer_yaml]
swagger_cli_validation(sw_cli_bundle_cmd)

validate_with_swagger_cli(bundled_test_consumer_yaml)

validate_with_openapi_spec(bundled_test_consumer_yaml)

def test_swagger_cli_validation_json():
    sw_cli_cmd = ['swagger-cli', 'validate', api_json]
    code, validity = swagger_cli_validation(sw_cli_cmd)
    # assert code == 0
    assert validity == True

def test_swagger_cli_validation_yaml():
    sw_cli_cmd = ['swagger-cli', 'validate', api_yaml]
    code, validity = swagger_cli_validation(sw_cli_cmd)
    # assert code == 0
    assert validity == True

def test_openapi_spec_validator_json():
    with open(api_json) as f:
        data = json.load(f)
    validate_spec(data)

def test_openapi_spec_validator_yaml():
    with open(api_yaml) as f:
        data = safe_load(f)
    validate_spec(data)

def test_swagger_cli_validation_bundled_yaml():
    sw_cli_cmd = ['swagger-cli', 'validate', bundled_test_consumer_yaml]
    code, validity = swagger_cli_validation(sw_cli_cmd)
    # assert code == 0
    assert validity == True

def test_openapi_spec_validator_bundled_yaml():
    with open(bundled_test_consumer_yaml) as f:
        data = safe_load(f)
    validate_spec(data)

print ("   validation process is finished")
