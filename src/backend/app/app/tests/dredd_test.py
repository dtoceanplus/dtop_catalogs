# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Module dredd_test
'''''''
The module performs the dredd testing of Swagger API against run backend

.. automodule:: core.dredd_test
"""
import os
import sys
import subprocess
from ruamel.yaml import YAML

filedir = os.path.dirname(os.path.realpath(__file__))
coredir = os.path.join(filedir, '../core/')

sys.path.append(coredir)
import appenv

endpoint_val="http://" + os.environ['CATALOG_API_HOST_IP'] + ":" + os.environ['CATALOG_API_PORT']

xls_name = os.environ['CATALOG_EXCEL_IMPORT_FILE']
xls_dir_name = os.path.splitext(xls_name)[0]
api_version = os.environ['API_VERSION']

blueprint_val = "../api/" + xls_dir_name + "/" + api_version + "/swagger.json"

if os.path.isfile(blueprint_val):
    print (blueprint_val + " exists")
else:
    print (blueprint_val + " doesn't exist")

print('creating test database from excel data')
import checktypes
import excel2db

print('updating dredd.yml')
def update_dredd_yml(key, value):
    yaml = YAML()
    yaml.default_flow_style = None
    with open('dredd.yml', 'r') as in_stream:
        conf = yaml.load(in_stream)
    in_stream.close()
    conf[key] = value
    with open('dredd.yml', 'w') as out_stream:
        yaml.dump(conf, out_stream)
    out_stream.close()

update_dredd_yml('endpoint', endpoint_val)
update_dredd_yml('blueprint', blueprint_val)

print('performing dredd testing')
def run_dredd():
    try:
      if (subprocess.getstatusoutput('dredd --help')[0] !=0):
          print("dredd is not installed")
          exit(1)
      if os.name == 'nt':
          # out = subprocess.run('dredd', capture_output=True, shell=True)
          subprocess.run('dredd', shell=True)
      else:
          with open('dredd-stdout.txt', 'w') as fw:
              subprocess.run('dredd', stdout=fw, stderr=subprocess.STDOUT)
          fw.close()

          with open('dredd-stdout.txt', 'r') as fr:
              lines = fr.readlines()
          fr.close()

          for line in lines:
              print(line.strip())

    except subprocess.CalledProcessError as e:
        if (e.returncode != 0):
            exit(1)
    return

run_dredd()

print('updating dredd.yml to initial state')
update_dredd_yml('endpoint', '')
update_dredd_yml('blueprint', '')

print ('dredd testing is finished')
