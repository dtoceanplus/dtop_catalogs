#
# This script exposes an existing database as a webservice.
# A lot of dirty things going on here because we have to handle all sorts of edge cases
#

import sys, logging, inspect, builtins, os, argparse, tempfile, atexit, shutil, io
from sqlalchemy import (
    CHAR,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Index,
    Integer,
    String,
    TIMESTAMP,
    Table,
    Text,
    UniqueConstraint,
    text,
)
from sqlalchemy.sql.sqltypes import NullType
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, redirect
from flask_swagger_ui import get_swaggerui_blueprint
from safrs import SAFRSBase, jsonapi_rpc, SAFRSJSONEncoder
from safrs import search, SAFRSAPI
from io import StringIO
from sqlalchemy.engine import create_engine
from sqlalchemy.schema import MetaData
from flask_cors import CORS
import pathlib
import pkg_resources

filedir = os.path.dirname(os.path.realpath(__file__))
coredir = os.path.join(filedir, '../../core/')

sys.path.append(coredir)

import appenv
from appenv import geturl

catalog_name = os.path.splitext(os.environ['CATALOG_EXCEL_IMPORT_FILE'])[0]
version = os.environ['API_VERSION']
descript="DTOP catalog  '" + catalog_name + "'  OpenAPI "
service_terms="https://www.dtoceanplus.eu/"
contact= {
    "name": "OpenCasCade DTOceanPlus team",
    # "url": "https://www.opencascade.com/contact",
    "email": "dtoceanplus.integration@opencascade.com"
}

consumes_val = ["application/json"]
produces_val = ["application/json"]

api_host = os.environ['CATALOG_API_HOST_IP']
api_port = os.environ['CATALOG_API_PORT']

MODEL_DIR = tempfile.mkdtemp() # directory where the generated models.py will be saved

sqlacodegen_dir = os.path.join(os.path.dirname(__file__), "sqlacodegen")
if not os.path.isdir(sqlacodegen_dir):
    print("sqlacodegen not found")

sys.path.insert(0, MODEL_DIR)
sys.path.insert(0, sqlacodegen_dir)

from sqlacodegen.codegen import CodeGenerator

def set_args():
    _parser = argparse.ArgumentParser()
    _parser.add_argument('-url')
    _parser.add_argument('-host')
    _parser.add_argument('-port')
    _parser.add_argument('--models')
    _parser.add_argument('-tables')
    _parser.add_argument('-schema')
    _parser.add_argument('-noviews')
    _parser.add_argument('-noindexes')
    _parser.add_argument('-noconstraints')
    _parser.add_argument('-nojoined')
    _parser.add_argument('-noinflect')
    _parser.add_argument('--outfile')

    connection_url = geturl()

    _args = _parser.parse_args(['-url', connection_url, \
      '-host', api_host, '-port', api_port, '-noviews', 'store_true', '-noindexes', 'store_true', '-noconstraints', 'store_true', \
      '-nojoined', 'store_true', '-noinflect', 'store_true'])

    return _args

def get_args():

    parser = argparse.ArgumentParser(description="Generates SQLAlchemy model code from an existing database.")
    parser.add_argument("url", nargs="?", help="SQLAlchemy url to the database")
    parser.add_argument("--version", action="store_true", help="print the version number and exit")
    parser.add_argument("--host", default="0.0.0.0", help="host (interface ip) to run")
    parser.add_argument("--port", default=5000, type=int, help="host (interface ip) to run")
    parser.add_argument("--models", default=None, help="Load models from file instead of generating them dynamically")
    parser.add_argument("--schema", help="load tables from an alternate schema")
    parser.add_argument("--tables", help="tables to process (comma-separated, default: all)")
    parser.add_argument("--noviews", action="store_true", help="ignore views")
    parser.add_argument("--noindexes", action="store_true", help="ignore indexes")
    parser.add_argument("--noconstraints", action="store_true", help="ignore constraints")
    parser.add_argument("--nojoined", action="store_true", help="don't autodetect joined table inheritance")
    parser.add_argument("--noinflect", action="store_true", help="don't try to convert tables names to singular form")
    parser.add_argument("--noclasses", action="store_true", help="don't generate classes, only tables")
    parser.add_argument("--outfile", help="file to write output to (default: stdout)")
    args = parser.parse_args()

    if args.version:
        version = pkg_resources.get_distribution("sqlacodegen").parsed_version
        print(version.public)
        exit()
    if not args.url:
        print("You must supply a url\n", file=sys.stderr)
        parser.print_help()
        exit(1)

    return args


def fix_generated(code):
    # if db.session.bind.dialect.name == "sqlite":
    #     code = code.replace("Numeric", "String")

    return code


def codegen(args):

    # Use reflection to fill in the metadata
    engine = create_engine(args.url)

    metadata = MetaData(engine)
    tables = args.tables.split(",") if args.tables else None
    metadata.reflect(engine, args.schema, not args.noviews, tables)

    # if db.session.bind.dialect.name == "sqlite":
    #     # dirty hack for sqlite
    #     engine.execute("""PRAGMA journal_mode = OFF""")

    # Write the generated model code to the specified file or standard output

    capture = StringIO()
    # outfile = io.open(args.outfile, 'w', encoding='utf-8') if args.outfile else capture # sys.stdout
    generator = CodeGenerator(
        metadata, args.noindexes, args.noconstraints, args.nojoined, args.noinflect
    )
    generator.render(capture)
    generated = capture.getvalue()
    generated = fix_generated(generated)
    if args.outfile:
        outfile = io.open(args.outfile, "w", encoding="utf-8")
        outfile.write(generated)
    return generated

def import_models():
    print(set_args())
    args = set_args()
    # args = get_args()
    app = Flask(descript)
    CORS(app, origins=["*"])

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    ## app.config.update(SQLALCHEMY_DATABASE_URI=args.url, DEBUG=True)
    app.config.update(SQLALCHEMY_DATABASE_URI=args.url, DEBUG=False)
    SAFRSBase.db_commit = False
    builtins.db = SQLAlchemy(app)  # set db as a global variable to be used
    models = codegen(args)

    models_file = os.path.join(MODEL_DIR, "models.py")
    init_file = os.path.join(MODEL_DIR, "__init__.py")

    #
    # Write the models to file, we could try to exec() but this makes our code more complicated
    # Also, we can modify models.py in case things go awry
    #
    if args.models:
        model_dir = os.path.basename(args.models)
        sys.path.insert(0, model_dir)
    else:
        with open(models_file, "w") as models_f:
            models_f.write(models)
            models_f.flush()
            os.fsync(models_f)
        models_f.close()

    with open(init_file, "w") as ini_f:
        ini_f.write("#")
        ini_f.flush()
        os.fsync(ini_f)
    ini_f.close()

    import imp
    models = imp.load_source('models', os.path.join(MODEL_DIR, 'models.py'))

    import inspect
    for name, obj in inspect.getmembers(models):
        if inspect.isclass(obj):
            print (obj)

    return app, models

def app_doc(app, models, HOST="0.0.0.0", PORT=5000):

    OAS_PREFIX = "/api"  # swagger prefix
    with app.app_context():
        api = SAFRSAPI(
            app,
            host=HOST,
            port=PORT,
            prefix=OAS_PREFIX,
            api_spec_url=OAS_PREFIX + "/swagger",
            schemes=["http", "https"],
            description="",
            api_version=version,
            contact_info=contact,
            term_of_service=service_terms,
            consumes = consumes_val,
            produces = produces_val
        )

        for name, model in inspect.getmembers(models):
            bases = getattr(model, "__bases__", [])

            if SAFRSBase in bases:
                # Create an API endpoint
                # Add search method so we can perform lookups from the frontend
                # model.search = search
                api.expose_object(model)

        return api.get_swagger_doc()


def start_api(app, models, HOST="0.0.0.0", PORT=5000):

    OAS_PREFIX = "/api"  # swagger prefix
    with app.app_context():
        api = SAFRSAPI(
            app,
            host=HOST,
            port=PORT,
            prefix=OAS_PREFIX,
            api_spec_url=OAS_PREFIX + "/swagger",
            schemes=["http", "https"],
            description="",
            api_version=version,
            contact_info=contact,
            term_of_service=service_terms,
            consumes = consumes_val,
            produces = produces_val
        )
        for name, model in inspect.getmembers(models):
            bases = getattr(model, "__bases__", [])

            if SAFRSBase in bases:
                # Create an API endpoint
                # Add search method so we can perform lookups from the frontend
                # model.search = search
                api.expose_object(model)

        @app.route("/")
        def goto_api():
            return redirect(OAS_PREFIX)

def gen_api():
    app, models = import_models()
    HOST = os.environ['CATALOG_API_HOST_IP']
    PORT = os.environ['CATALOG_API_PORT']
    return app_doc(app, models, HOST, PORT)

def start_db_api():
    app, models = import_models()
    HOST = api_host
    PORT = api_port
    start_api(app, models, HOST, PORT)
    print("API URL: http://{}:{}/api , model dir: {}".format(HOST, PORT, MODEL_DIR))
    print("API VESRION = " + version)
    app.run(host=HOST, port=PORT)

def create_app():
    app, models = import_models()
    HOST = api_host
    PORT = api_port
    start_api(app, models, HOST, PORT)
    print("API URL: http://{}:{}/api , model dir: {}".format(HOST, PORT, MODEL_DIR))
    print("API VESRION = " + version)
    return app

if __name__ == "__main__":
    start_db_api()
