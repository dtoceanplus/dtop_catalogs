#!/bin/bash

# This is the Catalogue Module for the DTOceanPlus suite of Tools.
# This module manages common Catalogues for DTOceanPlus modules.
# Copyright (C) 2021 OPEN CASCADE
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

usage () {
    cat <<HELP_USAGE

    The script sets the host name value to the <environment file>.
    "localhost" is used for tests in local environment.

    Usage :   $0 <host name> <environment file>

    example : $0 localhost ../../backend.env

HELP_USAGE
}

if [ $# -ne 2 ]
then
    usage
    exit 1
fi

if [[ ( $# == "--help") ||  $# == "-h" ]]
then
    usage
    exit 0
fi

backenv_file="$2"

cat ${backenv_file}

export $(grep -v "^#" $backenv_file | xargs)

cur_hostname="$1"

sed -i "s/CATALOG_API_HOST_IP=.*/CATALOG_API_HOST_IP=$cur_hostname/" $backenv_file

echo "----------------------------------"
echo "The host name has been set: "
echo " ----------------------------------"
cat ${backenv_file}
