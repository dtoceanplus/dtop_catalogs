SHELL = bash

include src/backend/backend.env

MODULE_NICKNAME = cm
CM_BACKEND_IMAGE = $(MODULE_NICKNAME):cm_backend

#CM_BACKEND_CONTAINER = $(MODULE_NICKNAME)_backend_container
CM_BACKEND_CONTAINER = $(MODULE_NICKNAME)-backend

build-cm_backend: # Build docker backend image
	# docker build --no-cache --tag $(CM_BACKEND_IMAGE) .
	docker build --cache-from $(CM_BACKEND_IMAGE) --tag $(CM_BACKEND_IMAGE) .

run_catalog = docker run --interactive --tty --detach --rm --name $(CM_BACKEND_CONTAINER) --entrypoint "bash" $(CM_BACKEND_IMAGE) -c "$1"

run-ed_cm_backend:  #Run docker container and backend server for ED module
	$(call run_catalog, ./run_ed_server.sh)

run-lmo_cm_backend:  #Run docker container and backend server for LMO module
	$(call run_catalog, ./run_lmo_server.sh)

run-et_cm_backend:  #Run docker container and backend server for ET module
	$(call run_catalog, ./run_et_server.sh)

run-sk_cm_backend:  #Run docker container and backend server for SK module
	$(call run_catalog, ./run_sk_server.sh)

run-slc_cm_backend:  #Run docker container and backend server for SLC module
	$(call run_catalog, ./run_slc_server.sh)

run-all_cm_backend:  #Run docker container and backend server for ALL modules
	$(call run_catalog, ./run_all_server.sh)

run_catalog_with_ports = docker run --interactive --tty --detach --rm --publish $(CATALOG_API_PORT):$(CATALOG_API_PORT) --name $(CM_BACKEND_CONTAINER) --entrypoint "bash" $(CM_BACKEND_IMAGE) -c "$1"

run-ed_cm_backend_local:  #Run docker container and backend server for ED module with port publish
	$(call run_catalog_with_ports, ./run_ed_server.sh)

run-lmo_cm_backend_local:  #Run docker container and backend server for LMO module with port publish
	$(call run_catalog_with_ports, ./run_lmo_server.sh)

run-et_cm_backend_local:  #Run docker container and backend server for ET module with port publish
	$(call run_catalog_with_ports, ./run_et_server.sh)

run-sk_cm_backend_local:  #Run docker container and backend server for SK module with port publish
	$(call run_catalog_with_ports, ./run_sk_server.sh)

run-slc_cm_backend_local:  #Run docker container and backend server for SLC module with port publish
	$(call run_catalog_with_ports, ./run_slc_server.sh)

run-all_cm_backend_local:  #Run docker container and backend server for ALL modules with port publish
	$(call run_catalog_with_ports, ./run_all_server.sh)

x-set_local_host:
	bash -c "cd src/backend/app/app; ./set_hostname.sh localhost ../../backend.env"

x-set_docker_host:
	bash -c "cd src/backend/app/app; ./set_hostname.sh 0.0.0.0 ../../backend.env"

set_catalog = bash -c "cd src/backend/app/app; ./set_catalog.sh $1 ../../backend.env"

x-set_ed_catalog: #Set the catalog for ED module
	$(call set_catalog, dtop-catalog-electrical_network_equipment.xlsx)

x-set_lmo_catalog: #Set the catalog for LMO module
	$(call set_catalog, dtop-catalog_operations_infrastructures_and_equipment.xlsx)

x-set_et_catalog: #Set the catalog for ET module
	$(call set_catalog, dtop-catalog-power_take_off_components.xlsx)

x-set_sk_catalog: #Set the catalog for SK module
	$(call set_catalog, dtop-catalog-mooring_lines_and_anchors.xlsx)

x-set_slc_catalog: #Set the catalog for SLC module
	$(call set_catalog, dtop-catalog_cost_benchmark.xlsx)

x-set_all_catalog: #Set the catalog for ALL modules
	$(call set_catalog, dtop-catalog.xlsx)

x-pre-commit: #Pre-commit processing
	pre-commit install
	pre-commit run --all-files

cm_backend_pytest: #Perform pytesting
	docker exec $(CM_BACKEND_CONTAINER) python core/genapi.py
	docker exec $(CM_BACKEND_CONTAINER) pytest -rxXs -o junit_family=xunit1 --cov=. --junitxml=report.xml core/checkapi.py

cm_backend_dredd: #Perform dredd testing
	docker exec $(CM_BACKEND_CONTAINER) python core/genapi.py
	docker exec $(CM_BACKEND_CONTAINER) bash -c 'cd tests; python dredd_test.py'

cm_backend_all_tests: #Perform all tests (both pytest and dredd testing)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

copy_artifacts: #copy all artifacts
#	docker cp $(CM_BACKEND_CONTAINER):/app/src/backend/app/app/report.xml .
	docker cp $(CM_BACKEND_CONTAINER):/app/src/backend/app/app/tests/dredd-stdout.txt .
	docker cp $(CM_BACKEND_CONTAINER):/app/api/${POSTGRESQL_DATABASE}/${API_VERSION}/openapi.yaml .

remove-cm_backend_container: #Stop and remove cm backend container
	docker rm -f $(CM_BACKEND_CONTAINER) || true

remove-cm_backend_image: #Stop and remove cm backend image
	docker image rm -f $(CM_BACKEND_IMAGE)

test-pre-commit: build-cm_backend test-pre-commit-nodeps
test-pre-commit-nodeps: #For pre-commit test
	docker run --interactive --tty --detach --rm --name $(CM_BACKEND_CONTAINER) --entrypoint "bash" $(CM_BACKEND_IMAGE) -c "make x-pre-commit"

test-ed_cm_backend: build-cm_backend test-ed_cm_backend-nodeps
test-ed_cm_backend-nodeps: #For complete GitLab CI Job for ED module
	$(call run_catalog, ./run_ed_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-lmo_cm_backend: build-cm_backend test-lmo_cm_backend-nodeps
test-lmo_cm_backend-nodeps:  #For complete GitLab CI Job for LMO module
	$(call run_catalog, ./run_lmo_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-et_cm_backend: build-cm_backend test-et_cm_backend-nodeps
test-et_cm_backend-nodeps:  #For complete GitLab CI Job for ET module
	$(call run_catalog, ./run_et_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-sk_cm_backend: build-cm_backend test-sk_cm_backend-nodeps
test-sk_cm_backend-nodeps:  #For complete GitLab CI Job for SK module
	$(call run_catalog, ./run_sk_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-slc_cm_backend: build-cm_backend test-slc_cm_backend-nodeps
test-slc_cm_backend-nodeps:  #For complete GitLab CI Job for SLC module
	$(call run_catalog, ./run_slc_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-all_cm_backend: build-cm_backend test-all_cm_backend-nodeps
test-all_cm_backend-nodeps:  #For complete GitLab CI Job for ALL modules
	$(call run_catalog, ./run_all_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-ed_cm_backend-win10: build-cm_backend test-ed_cm_backend-win10-nodeps
test-ed_cm_backend-win10-nodeps: #For complete GitLab CI Job for ED module
	docker rm -f $(CM_BACKEND_CONTAINER) || true
	$(call run_catalog, ./run_ed_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-lmo_cm_backend-win10: build-cm_backend test-lmo_cm_backend-win10-nodeps
test-lmo_cm_backend-win10-nodeps:  #For complete GitLab CI Job for LMO module
	docker rm -f $(CM_BACKEND_CONTAINER) || true
	$(call run_catalog, ./run_lmo_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-et_cm_backend-win10: build-cm_backend test-et_cm_backend-win10-nodeps
test-et_cm_backend-win10-nodeps:  #For complete GitLab CI Job for ET module
	docker rm -f $(CM_BACKEND_CONTAINER) || true
	$(call run_catalog, ./run_et_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-sk_cm_backend-win10: build-cm_backend test-sk_cm_backend-win10-nodeps
test-sk_cm_backend-win10-nodeps:  #For complete GitLab CI Job for SK module
	docker rm -f $(CM_BACKEND_CONTAINER) || true
	$(call run_catalog, ./run_sk_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-slc_cm_backend-win10: build-cm_backend test-slc_cm_backend-win10-nodeps
test-slc_cm_backend-win10-nodeps:  #For complete GitLab CI Job for SLC module
	docker rm -f $(CM_BACKEND_CONTAINER) || true
	$(call run_catalog, ./run_slc_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh

test-all_cm_backend-win10: build-cm_backend test-all_cm_backend-win10-nodeps
test-all_cm_backend-win10-nodeps:  #For complete GitLab CI Job for ALL modules
	docker rm -f $(CM_BACKEND_CONTAINER) || true
	$(call run_catalog, ./run_all_server.sh)
	docker exec $(CM_BACKEND_CONTAINER) ./run_tests.sh


all-catalog-up: x-set_docker_host
	$(call set_catalog, dtop-catalog.xlsx)
	docker-compose up --detach cm-nginx cm-backend cm-frontend

all-catalog-down:
	docker-compose down
