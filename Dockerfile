FROM python:3.7

RUN apt-get update \
    && apt-get install postgresql postgresql-client libpq-dev -y \
    && apt-get install systemd -y \
    && apt-get install netcat -y \
    && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs

RUN npm install --global swagger-cli \
    && npm install --global api-spec-converter \
    && npm install --global prettier \
    && npm install --global dredd@12.2.1

RUN (cd /usr/ && curl https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh --output wait-for-it.sh && chmod +x wait-for-it.sh)

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

WORKDIR /app/src/backend/app/app

RUN chmod a+x *.sh

CMD bash
