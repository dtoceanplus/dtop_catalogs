# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

from setuptools import find_packages, setup

setup(
    name="dtop_catalogs",
    version="0.0.1",
    install_requires=[
        "flask",
        "flask-cors",
        "flask-restful",
        "flask-restful-swagger-2",
        "flask-sqlalchemy",
        "flask-swagger-ui",
        "openapi-spec-validator",
        "openpyxl",
        "pandas",
        "psycopg2",
        "pylint",
        "pytest",
        "python-dotenv",
        "pyyaml",
        "sqlalchemy",
        "sqlalchemy-utils",
        "xlrd"
    ],
    extras_require={
      "test": [
        "pytest",
        "dredd-hooks",
        "ruamel-yaml",
        "pytest-cov",
        "pre-commit"
      ]
    },
    python_requires=">=3.7.0",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    zip_safe=False,
)
