.. _cm-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Catalog module.
They are intended for those who are new to the Catalog modume.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

#. :ref:`cm-browse-catalogs`
#. :ref:`cm-edit-catalogs`

.. toctree::
   :maxdepth: 1
   :hidden:

   browse_catalogs
   edit_catalogs
