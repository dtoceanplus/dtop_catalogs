.. _cm-browse-catalogs: 

Browse Catalogs
===============

The Catalog module is accessible from the Main Application.

Select Catalog
--------------

The available Catalogs are displayed in the form of a Tree.

.. image:: ../_static/Catalogs.jpg
  :alt: Catalogs
  :align: center
  :scale: 80%

To select a Catalog, you can either browse the tree, by unfolding the nodes,
or use the *Filter by catalogs* field to reduce the list.

For example, you can type *moo* to access quickly to the Mooring Lines catalog.

.. image:: ../_static/FilterCatalog.jpg
  :alt: Filter Catalogs
  :align: center
  :scale: 80%


Display Catalog Item
--------------------

Once you have selected the Catalog, you will see its items listed in a table.

.. image:: ../_static/moorings.jpg
  :alt: Mooring Lines
  :align: center
  :scale: 80%

To view a specific item click the ``Open`` button to view all its characteristics.

.. image:: ../_static/mooring_item.jpg
  :alt: Example of Mooring Line
  :align: center
  :scale: 80%


