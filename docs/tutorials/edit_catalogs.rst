.. _cm-edit-catalogs: 

Edit Catalogs
=============

The Catalogs module allows the user to edit the calags values.

.. warning::
   Editing or deleting catalogs' values may have an impact on projects
   where these values are already been used.


Create values
-------------

To create a new value in a catalog:

#. Select the catalog.
#. Click the ``New Value`` button in the top right corner.
#. A dialog is displayed with all the characteristics of this catalog.

   .. image:: ../_static/create_value.jpg
     :alt: Create Value
     :align: center

#. Enter the characteristics of the new value in the corresponding fields,
   and validate by clicking the ``Create`` button at the bottom of the dialog.

#. The new value is added to the Catalog.

Edit values
-----------

The process to edit a value is similar to the creation.

#. Select the catalog.
#. Browse to the value to edit.
#. Click the ``Edit`` button at the end of the line.
#. An edit dialog is displayed with all the characteristics of the selected value.

   .. image:: ../_static/edit_value.jpg
     :alt: Edit Value
     :align: center

#. Validate by clicking the ``Edit`` button at the bootom of the dialog.
#. The value is updated in the list.

Delete values
--------------

To delete a value in a catalog.

#. Select the catalog.
#. Browse to the value to delete.
#. Click the ``Delete`` button àt the end of the line.
#. A confirmation dialog is displayed. Confirm the deletion by clicking the ``Delete`` button on this dialog.

   .. image:: ../_static/delete_value.jpg
     :alt: Delete Value
     :align: center
     :scale: 80%
