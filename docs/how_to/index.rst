.. _cm-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Catalogue module. 
These guides are intended for users who have previously completed all the :ref:`Catalogue tutorials <cm-tutorials>` and have a good knowledge of the features and workings of the Catalogue module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

#. :ref:`cm-browse-values`

.. toctree::
   :maxdepth: 1
   :hidden:

   browse_values
