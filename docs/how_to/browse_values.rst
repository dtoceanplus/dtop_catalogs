.. _cm-browse-values:

Browse a catalog's values
=========================

When a catalogs as many values, it can be difficult to find a specific value. 
To siplify the search of values, the catalog page offers the following features.

Show/Hide columns
-----------------

By default, all the characteristics of the catalog's values are displayed in columns.
You can use the slider at the bottom of the list to navigate to all the column.
But it can be sometimes difficult to compare several items

You can add or remove columns using the *Show/Hide Columns* widget.

#. Click on the *Show/Hide Columns* label to expand the widget.
#. All the available characteristics of this catalog are displayed with toggle buttons.
#. Enable or disable the columns you want to show or hide.

.. image:: ../_static/show_column.jpg
  :alt: Show/Hide columns
  :align: center


Use columns' filters
--------------------

You can reduce the list of values displayed by using the filters available on top of each column.

#. Type a filter in one or several columns.
#. Click the ``Filter`` button on the right.
#. The entered filters are applied, and only the values matching the requested criteria are displayed.

   .. image:: ../_static/filter_columns.jpg
     :alt: Filter columns
     :align: center

#. Click the ``Reset`` button on the right to clear the filter and display all the values again.

Sort columns
------------

Use the *Up* and *Down* icons on each column to sort the list of values with this column.

.. image:: ../_static/sort_columns.jpg
  :alt: Sort columns
  :align: center

Page navigation
---------------

For performances reasons, the values are displayed by pages.

Use the navigation bar at the bottom left of the page to naviage through all the values of the catalog.

The default number of values per page is *10*.

You can display more or less values by changing the value in the combo box at the bottom right of the page.