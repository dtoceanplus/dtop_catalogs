.. _cm-home:

Catalog
=======

Introduction 
------------

The Catalog module (CM) is a special module which manages generic list of values
like mooring lines, anchors, vessels...

These values will be used in the frame of the Design modules to define the project
with real characteristics, and by the Assessment modules to compute metrics.

The Catalog module allows the user to browse the values to check the full characteristics
of each item, and also to extend each catalogs by creating, editing and deleting values.

Structure
---------

This module's documentation is divided into three main sections:

- :ref:`cm-tutorials` to give step-by-step instructions on using CM for new users. 

- :ref:`cm-how-to` that show how to achieve specific outcomes using CM. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index

