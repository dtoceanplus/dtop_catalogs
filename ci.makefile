# This file is intended primarily for CI.
# Unlike default Makefile which works like this: 'build, test',
# this file performs more steps: 'pull, build, push, test'.

SHELL = bash
.PHONY: *

MODULE_NICKNAME = cm
REGISTRY_PORT = 5500

CI_REGISTRY_IMAGE ?= localhost:$(REGISTRY_PORT)/$(MODULE_NICKNAME)

# Convenient default target that simulates
all: \
	registry-start \
	update-dind update-cm_backend \
	test-pre-commit test-ed_cm_backend test-lmo_cm_backend test-et_cm_backend test-sk_cm_backend test-slc_cm_backend test-all_cm_backend
	@:

clean: registry-remove
	docker rmi --force $(shell docker images --filter "reference=${CI_REGISTRY_IMAGE}" --quiet) | true
	docker rmi --force $(shell docker images --filter "reference=${MODULE_NICKNAME}" --quiet) | true

# Start local Docker registry if it is not yet started.
registry-start:
	if [ ! $(shell docker ps --filter "name=registry-$(MODULE_NICKNAME)" --format="{{.ID}}" --no-trunc) ]; then \
		docker run --detach --publish $(REGISTRY_PORT):5000 --restart always --name registry-$(MODULE_NICKNAME) registry:2; \
	fi

# Stop local Docker registry if it is running.
registry-remove:
	docker container stop registry-$(MODULE_NICKNAME) | true
	docker container rm --volumes registry-$(MODULE_NICKNAME) | true

# Convenient target for preparation before running a test.
update-%:
	$(MAKE) -f ci.makefile pull-$*
	$(MAKE) -f ci.makefile build-$*
	$(MAKE) -f ci.makefile push-$*

# Pull image from Docker registry and tag it according to a certain convention.
# If pull fails, it is not an error: maybe we are just working with a fresh registry.
pull-%:
	docker pull $(CI_REGISTRY_IMAGE):$* \
	&& docker tag $(CI_REGISTRY_IMAGE):$* $(MODULE_NICKNAME):$* \
	|| true

# Run 'build' step from Makefile.
# It is expected that 'pull' is done before this, so Docker Compose will reuse some or all layers.
build-%:
	$(MAKE) $@

# Special case for building Docker-in-Docker image for CI
build-dind:
	docker build --cache-from $(MODULE_NICKNAME):dind --tag $(MODULE_NICKNAME):dind --file ci/dind.dockerfile .

# Push image to Docker registry.
push-%:
	docker tag $(MODULE_NICKNAME):$* $(CI_REGISTRY_IMAGE):$*
	docker push $(CI_REGISTRY_IMAGE):$*

# Pull image and immediately run the actual test.
%: pull-%
	$(MAKE) $@-nodeps

pull-test-pre-commit: pull-cm_backend
	@:

pull-test-ed_cm_backend: pull-cm_backend
	@:

pull-test-lmo_cm_backend: pull-cm_backend
	@:

pull-test-et_cm_backend: pull-cm_backend
	@:

pull-test-sk_cm_backend: pull-cm_backend
	@:

pull-test-slc_cm_backend: pull-cm_backend
	@:

pull-test-all_cm_backend: pull-cm_backend
	@:


## !
## Set the module nickname
MODULE_SHORT_NAME=cm

## !
## Set the module docker image TAG
CM_IMAGE_TAG=1.0.0

## !
## Set the value to GitLab container registry of the module
CM_CI_REGISTRY=registry.gitlab.com/dtoceanplus/dtop_catalogs

## !
## Set DTOP Basic Authentication Header if necessary.
## That is Base64 encoded values of username: and password: that are requested for initial access to modules
## For example: username: "admin@dtop.com", password: "j2zjf#afw21"
## "Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE="
DTOP_BASIC_AUTH=Basic YWRtaW5AZHRvcC5jb206ajJ6amYjYWZ3MjE=

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CM_CI_REGISTRY}

logout:
	docker logout ${CM_CI_REGISTRY}

build-prod-be:
	docker pull ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${CM_IMAGE_TAG} || true
	docker build --cache-from ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${CM_IMAGE_TAG} \
	  --tag ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${CM_IMAGE_TAG} \
          --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          ./
	docker push ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${CM_IMAGE_TAG}
	docker images

build-prod-fe:
	docker pull ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${CM_IMAGE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	  --cache-from ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${CM_IMAGE_TAG} \
	  --tag ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${CM_IMAGE_TAG} \
          --file ./src/frontend/app/app/cat/frontend-prod.dockerfile \
	  ./
	docker push ${CM_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${CM_IMAGE_TAG}
	docker images
