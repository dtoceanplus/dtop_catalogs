# Overview

The main development objective is to provide a generic approach for maximally automated creation of Catalogs from Excel file data sources. This approach includes a generic Back End and Front End services. Each XLS Catalog could be considered as a separate module/deliverable. So, it could be advanced, distributed and populated on its own. Catalogs could be coupled with other Tools via REST API. For now, the Catalogs from the following module owners are processed :
- ED - [dtop-catalog-electrical_network_equipment.xlsx](https://gitlab.com/dtoceanplus/dtop_catalogs/-/blob/master/src/backend/app/app/data/dtop-catalog-electrical_network_equipment.xlsx), source file - `_initial_ED_catalogue_v3.xlsx` (6 sheets)
- LMO - [dtop-catalog_operations_infrastructures_and_equipment.xlsx](https://gitlab.com/dtoceanplus/dtop_catalogs/-/blob/master/src/backend/app/app/data/dtop-catalog_operations_infrastructures_and_equipment.xlsx), source file - `_initial_LMO_Catalogue_20072021.xlsx` (12 sheets)
- ET - [dtop-catalog-power_take_off_components.xlsx](https://gitlab.com/dtoceanplus/dtop_catalogs/-/blob/master/src/backend/app/app/data/dtop-catalog-power_take_off_components.xlsx), source file - `_initial_ET_Catalogue_22102020.xlsx` (6 sheets)
- SK - [dtop-catalog-mooring_lines_and_anchors.xlsx](https://gitlab.com/dtoceanplus/dtop_catalogs/-/blob/master/src/backend/app/app/data/dtop-catalog-mooring_lines_and_anchors.xlsx), source file - `_initial_sk_catalogue.xlsx` (2 sheets)
- SLC - [dtop-catalog_cost_benchmark.xlsx](https://gitlab.com/dtoceanplus/dtop_catalogs/-/blob/master/src/backend/app/app/data/dtop-catalog_cost_benchmark.xlsx), source file - `_initial_slc_benchmark.xlsx` (1 sheet)
- ALL - [dtop-catalog.xlsx](https://gitlab.com/dtoceanplus/dtop_catalogs/-/blob/master/src/backend/app/app/data/dtop-catalog.xlsx) with all catalog Excel files merged into one common Excel file (27 sheets)
- Besides, the sheet `desc_units` containing units and description for all comulns of all sheet catalogs is introduced. For more easy tracking the correspondent incoming updates of "desc & units" data one file using date suffix is also supported, for example "columns_unit_desc_2021_07_27.xlsx".

Generic Catalog Module (CM) implementation is based on using external frameworks. So, CM  implementation robustness can be implicitly derived by the frameworks it is based on, namely - flask, sqlacodegen and safrs.

The next naming is used below :
- `<WORKING_DIR>=<your path>/dtop_catalogs>` - Root directory of Catalog module clonned from Gitlab Project repository.
- `<BACKEND>=<WORKING_DIR>/src/backend/app/app` - Back End directory
- `<BACKEND>=<WORKING_DIR>/src/frontend/app/app` - Front End directory

A repository presents the dtop catalogs development and utilities for
- check of Data Types for source Excel file
- check, creation and drop of dtop catalog database in PostgreSQL database
- import of data from Excel file presenting dtop catalog with data types from JSON file to PostgreSQL database
- export of catalog data from PostgreSQL database to Excel file
- generation of SQLAlchemy ORM data model for catalog data from PostgreSQL database
- running of Flask service that exposes OpenAPI/Swagger-generated API for the catalog data in database
- generation of OpenAPI/Swagger API in `api/<api version>/<catalog name>` of `<BACKEND>` directory
- check of the generated OpenAPI using the validators
- copy/publication of the generated OpenAPI to `api-cooperative` directory `<WORKING_DIR>/api/<api version>/<catalog name>` created as git `api` submodule.
- performing the dredd testing of Swagger API against run Flask service

Finally git api submodule with newly generated OpenAPI `openapi.yaml` should be pushed to `api-cooperative` git repository.

# Contribution of additional Catalogs Data

## Catalogs Data delivery

Additional Catalogs Data and updates of existing Catalogs are provided by DTOP module responsible as Excel files populated with Data in the agreed format.

### General notes

Catalog Data on every Sheet of source Excel file should include the mandatory unique `id` of integer type in the first column.

`checktypes` module provides check of Column Data Types for every sheet of source Excel file. If a column contains data of different types (more than one), `checktypes` reports about Mixed Data types for such columns. Besides, it generates `<Excel file name>.json` file that defines `JSON schema` data types that will be used for correspondent Catalog Open API.

Basing on analysis of this info DTOP module responsible can adjust content of the source Excel file and repeat check with `checktypes` module if necessary.

Please follow [10 Rules for a Better SQL Schema](https://www.sisense.com/blog/better-sql-schema/).

### Steps to provide and test Catalogs' updates

The following workflow and steps are used to add, check and test the catalog updates.
It can be done in the next environments as described in the details in REAME :
- **Local files development environment**
- **Local development environment using Docker and Make**

The sequence of the actions below is based on **Local development environment using Docker and Make**.
The presented steps uses the `ET_Catalogue_202002.xlsx` as example. `dtop-catalog-power_take_off_components.xlsx` is ET input file with the agreed fixed name.

1. To keep tracking of the delivered Excel files, put newly delivered one into directory `src\backend\app\app\data` with following name : `_initial_ET_Catalogue_22102020.xlsx` that is just the renamed `ET_Catalogue_22102020.xlsx` with prefix `_initial_`.
2. Merge `ET_Catalogue_22102020.xlsx` into the ET file `dtop-catalog-power_take_off_components.xlsx` keeping the existing correct formats and data.
3. Preliminarily carefully check and adjust the newly introduced names of sheets and columns as well as all data in the `dtop-catalog-power_take_off_components.xlsx`.
4. Perform the tests and checks the correctness of `dtop-catalog-power_take_off_components.xlsx` using the generated json datatype file `dtop-catalog-power_take_off_components.json` and ET Catalog OpenAPI, for example like this.
  - Edit `docker-compose.yml` to set the required Catalog :
```
#    command: bash -c "./run_all_server.sh"
    command: bash -c "./run_et_server.sh"
```
  - Run docker-compose services :
```
make x-set_docker_host  (executed once)
docker-compose up --detach cm-nginx cm-backend cm-frontend
docker-compose down     (to stop)
```
  - Analyze the generated `<module catalog>.json`
```
src/backend/app/app/data/dtop-catalog-electrical_network_equipment.json
```
  - Perform and check results of dredd tests
```
docker exec cm-backend ./run_tests.sh
```
  - Perform the tests in the browser using one of the URLs :
```
http://localhost
or
http://cm.dtop.localhost
```
    - The generated file `data\dtop-catalog-power_take_off_components.json` describes JSON data types to be used in Open API. This step can be performed interactively to get expected correct results.
    - The generated files `api\dtop-catalog-electrical_network_equipment\0.0.1\openapi.yaml` file presents ET catalog OpenAPI.

5. Finally, if the tests confirm expected results, merge the correspondent sheets of the updated `dtop-catalog-power_take_off_components.xlsx` into common `dtop-catalog.xlsx`and perform the similar OpenAPI tests. Catalog database and Open API are automatically generated basing on this common Excel file.
  - Edit `docker-compose.yml` to set the required Catalog `dtop-catalog.xlsx` :
```
    command: bash -c "./run_all_server.sh"
#    command: bash -c "./run_et_server.sh"
```
  - Run docker-compose services :
```
docker-compose up --detach cm-nginx cm-backend cm-frontend
docker-compose down     (to stop)
```
  - Check the generated `<module catalog>.json`
```
src/backend/app/app/data/dtop-catalog.json
```
  - Perform and check results of dredd tests
```
docker exec cm-backend ./run_tests.sh
```
  - Perform the tests in the browser.
6. If a number of the sheets or their names were changed, update Catalog FE file `frontend\app\app\cat\src\views\HomePage.vue` and test Catalog FE.
7. If necessary update description and unit data on the sheet `desc_units` of `dtop-catalog.xlsx`.
8. Update REAME.md with actual info about the number of the sheets and name of newly provided Excel file, for example `_initial_ET_Catalogue_22102020.xlsx`.
9. Commit all changed and re-generated the catalog update files.
10. Create Merge Request to Catalog `master`.
11. Make Merge Request to `api-cooperative` master to publish the changed generated Catalog OpenAPI files.
12. Perform the final tests using  DTOP installation `dtop_inst` repository. CI build process creates Catalog FE and BE production images, that are used in the deployment stack of DTOP installation.

### Checking Data Types of source or updated Catalog Excel files

As mentioned above, the checking data types is provided by the execution of `python core/checktypes.py` generating the file `<module catalog>.json`, for example
`dtop-catalog-power_take_off_components.json`, that describes JSON data types to be used in Open API.

# Environment variables

All input Back End Environment variables are defined in the file :

`<WORKING_DIR>/src/backend/backend.env`

Set the necessary environment variables in particular :
- (*) `CATALOG_EXCEL_IMPORT_FILE=<Catalog Excel file name>` - source Excel file, for example, `dtop-catalog_operations_infrastructures_and_equipment.xlsx`
- (*) `CATALOG_EXCEL_EXPORT_FILE=<Catalog Excel file for export>` for example, `dtop-catalog_operations_infrastructures_and_equipment_exported`
- `POSTGRESQL_DATABASE=<Database name>` - PostgreSQL database name, for example, `dtop-catalog_operations_infrastructures_and_equipment`
- `CATALOG_API_HOST_IP=<HOST or IP>` - Catalog OpenAPI Host name or IP, for example, `localhost`
- `CATALOG_API_PORT=<PORT>` - Catalog OpenAPI Port, for example, `7777`
- (**) `TEST_CONSUMER=<Catalog test consumer>` - Catalog Test Consumer (ctc) OpenAPI, for example, `ctc_dtop-catalog_operations_infrastructures_and_equipment`

Directories and files location :

`<BACKEND>` - <WORKING_DIR>\src\backend\app\app

(*) - `<BACKEND>\data`

(**) - `<BACKEND>\api\dtop-cat-consumer\0.0.1`

# GitLab CI/CD

[GitLab CI/CD](https://docs.gitlab.com/ce/ci/)

The [.gitlab-ci.yml](.gitlab-ci.yml) file defines the CI (Continuous Integration) tasks that are automatically performed on every commit to the repository code with `git push` command. GitLab CI jobs provide the pytest, dredd, frontend and integration tests for all catalogs included in the repository.

The implementation of GitLab CI corresponds to the wiki [Makefile-based CI](https://gitlab.com/opencascade/dtocean/wiki/-/blob/master/dtoceanplus/Makefile-based-CI.md).

# Local development environment installation
Prerequisites for local development environment on Windows 10 Pro:
1. [Git Bash](https://gitforwindows.org/) - ( check that `C:\Program Files\Git\bin` in $PATH )
2. [miniconda3](https://docs.conda.io/en/latest/miniconda.html) - ( Python 3.7 : Miniconda3 Windows 64-bit )
3. make - ( `conda install -c conda-forge make` )
4. [PostgreSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) - ( Version 11.8 : Windows x86-64 )
5. pipenv - ( `conda install -c conda-forge pipenv` )
6. nodejs and npm - ( `conda install -c conda-forge nodejs` )
7. [swagger-cli](https://www.npmjs.com/package/swagger-cli)
8. [api-spec-converter](https://www.npmjs.com/package/api-spec-converter)
9. [prettier](https://www.npmjs.com/package/prettier)
10. [dredd](https://dredd.org/en/latest/)
11. [pre-commit](https://pre-commit.com/)

For a locally running PostgreSQL the database is created, dropped and checked using `core/dbutils.py` python script using environment variables defined in in `<WORKING_DIR>/src/backend/backend.env` - see details below.

For defining an environment Pipenv is used as the official package management tool recommended by Python itself.

`pipenv lock` is used to create a `Pipfile.lock` from Pipfile, which declares all dependencies (and sub-dependencies), their latest available versions, and the current hashes for the downloaded filters.

This operation is optional and should be executed when Pipfile file and environment are changed :

```bash
pipenv lock
```

Create pipenv virtual environment defined by correspondent `Pipfile.lock` file.

For production + development environment :
```bash
pipenv shell
pipenv install --dev --ignore-pipfile
```

Or for production environment :
```bash
pipenv shell
pipenv install --ignore-pipfile
```

Generate the requirements files `requirements.txt` (prod + dev) and `prod-requirements.txt` from the correspondent installed pipenv environment. This operation is optional and should be executed when Pipfile file and environment are changed :
```bash
bash -c "pipenv run pip freeze > requirements.txt"
bash -c "pipenv run pip freeze > prod-requirements.txt"
```

Install the necessary npm packages used for local tests :
```bash
npm install --global swagger-cli

npm install --global api-spec-converter

npm install --global prettier

npm install --global dredd@12.2.1
```

To remove pipennv virtualenv :
```bash
pipenv --rm
```

During development before `git push` it is recommended to process the files by `pre-commit-hooks` as configured in `.pre-commit-config.yaml`, for example :

```bash
# See https://pre-commit.com for more information
# See https://pre-commit.com/hooks.html for more hooks
repos:
    -   repo: https://github.com/pre-commit/pre-commit-hooks
        rev: v2.0.0
        hooks:
        -   id: trailing-whitespace
        -   id: check-added-large-files
        -   id: end-of-file-fixer
```

For that run commands :
```bash
pip install pre-commit
pre-commit run --all-files
pip uninstall pre-commit
```

Make sure that npm installation directory is added to PATH environment variable.

For windows example, npm installation directory is C:\Users\\\<user\>\AppData\Roaming\npm

Getting submodule sources :
```bash
git submodule update --init --recursive --remote
```

# Usage
## Backend testing
### **Local files development environment**

Install prerequisites and virtual requirement :
```bash
pipenv shell

pipenv install --dev --ignore-pipfile
pipenv install --ignore-pipfile
```

Using the correspondent `make` targets :

Set the local host name :

```shell
make x-set_local_host
```

Change / Set the required catalog in local environment :

Using the correspondent `make` targets :
```shell
make x-set_<catalog module>_catalog
```

where <catalog module> is one of `ed, lmo, et, sk, slc, all` for example :
```shell
make x-set_lmo_catalog
```

Perform the following commands.
```bash
cd <${BACKEND}>

python core/dbutils.py check_db | create_db  |  drop_db

python core/checktypes.py

python core/excel2db.py

python core/db2excel.py (optional)

python core/genmodel.py (optional)

python core/runapi.py
```

For local tests of catalog backend in the browser open web UI of the Catalog API using URL :
```
http://localhost:7777
or
http://cm.dtop.localhost:7777
or
http://your_hostname:7777
```

After running Flask service the Single Page Application (SPA) web UI (Swagger-UI) with exposed Swagger API (GET, POST, PATCH, DELETE) can be used for online local tests of retriving and updating data for every dedicated database table.

http://localhost:7777/api

The unit and dredd tests can be also performmed for run server from another console using the python scripts below.
```bash
pipenv shell

cd <${BACKEND}>

python core/genapi.py

python core/checkapi.py

pytest -rxXs -o junit_family=xunit1 --cov=. --junitxml=report.xml core/checkapi.py

cd tests

python dredd_test.py

cd ..

python core/excel2db.py

python core/copyapi.py
```

### **Local development environment using Docker and Make**

Set 0.0.0.0 host name :
```shell
make x-set_docker_host
```

Build docker backend image :
```bash
make build-cm_backend
```

Run docker container and backend server for the required catalog using the correspondent `make` targets :
```shell
make run-<catalog module>_cm_backend
```

where <catalog module> is one of `ed, lmo, et, sk, slc, all`, for example :
```shell
make run-lmo_cm_backend
```

To run docker container and backend server with port publishing use the next `make` targets :
```shell
make run-<catalog module>_cm_backend_local
```

Perform pytesting :
```bash
make cm_backend_pytest
```

Perform dredd testing :
```bash
make cm_backend_dredd
```

Perform all tests (both pytest and dredd testing) :
```bash
make cm_backend_all_tests
```

Copy artifacts :
```bash
make copy_artifacts
```

Stop and remove cm backend container
```bash
make remove-cm_backend_container
```

Stop and remove cm backend image
```bash
make remove-cm_backend_image
```

## Catalog Frontend with Backend testing
### Manual testing using docker-compose and make

#### Start
To start `docker-compose` services `nginx`, `backend` and `frontend` :
```shell
make all-catalog-up
```

#### Stop
To stop the `docker-compose` services :
```shell
make all-catalog-down
```

The tests of Catalog module Frontend with Backend can be performed in the browser using one of the URLs :
```
http://localhost
or
http://cm.dtop.localhost
or
http://your_hostname
```

### Cypress Testing
Now you can start e2e integration tests via Cypress.
```shell
docker-compose build
```

```shell
docker-compose up --detach
```

```shell
docker exec e2e-cypress npx cypress run
```


# Using dtop catalog Open API by another modules

## Introduction

To check integration and using dtop catalog Open API by another modules :
- the catalog test consumer (ctc) Open API files have been created, see
```shell
<BACKEND>/api/dtop-cat-consumer/0.0.1/ctc_dtop-catalog-electrical_network_equipment.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/ctc_dtop-catalog_operations_infrastructures_and_equipment.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/ctc_dtop-catalog-power_take_off_components.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/ctc_dtop-catalog-mooring_lines_and_anchors.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/ctc_dtop-catalog_cost_benchmark.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/ctc_dtop-catalog.yaml
```

As result of `checkapi.py` tests, the `bundled` test OpenAPI files are generated :
```shell
<BACKEND>/api/dtop-cat-consumer/0.0.1/bundled_ctc_dtop-catalog-electrical_network_equipment.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/bundled_ctc_dtop-catalog_operations_infrastructures_and_equipment.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/bundled_ctc_dtop-catalog-power_take_off_components.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/bundled_ctc_dtop-catalog-mooring_lines_and_anchors.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/bundled_ctc_dtop-catalog_cost_benchmark.yaml
<BACKEND>/api/dtop-cat-consumer/0.0.1/bundled_ctc_dtop-catalog.yaml
```

## Interaction with catalog module

The next ways for interaction with catalog module are provided using :
- Catalog module Front End
- Direct Back Ends communication : a module backend calls the catalog module backend

### Direct backends communication

A module backend can call directly the catalog module backend using HTTP(S) requests.

In request you can set a query string defining the required fields value's only (filters) and sort order.

You can see exact syntax on Swagger-UI SPA for correspondent catalog and Rest operation and Request URL.

For example, for database data imported from the sheet `dry_mate` of the Excel source file `ED_catalogue_v2.xlsx`


`fields[DryMate] defines the DryMate fields to include (csv)`

The next GET request defines the response/return only for 5 of 25 exiting fields of the dry_mate data :

id,mass,height,width,depth,non_critical_failure_max

```shell
http://localhost/api/dry_mate?fields[DryMate]=id,mass,height,width,depth,non_critical_failure_max
or
http://cm.dtop.localhost/api/dry_mate?fields[DryMate]=id,mass,height,width,depth,non_critical_failure_max
or
http://<host name>/api/dry_mate?fields[DryMate]=id,mass,height,width,depth,non_critical_failure_max
```

The test code `get_catalog_data.py` provides the example how to get `key - value` data for the required catalog as a python dictionary (get one row) or a list (get all rows).
```shell
cd <${BACKEND}>

python core/get_catalog_data.py --help

usage: get_catalog_data.py [-h] --endpoint ENDPOINT

optional arguments:
  -h, --help           show this help message and exit
  --endpoint ENDPOINT  endpoint of run catalog server
```

For example :

```shell
python core/get_catalog_data.py --endpoint http://localhost:7777/api/dry_mate/71
```
It can be executed from running container :

```shell
docker exec cm-backend python core/get_catalog_data.py --endpoint http://localhost:7777/api/dry_mate/71
```

Response :

```shell
<Response [200]>
response is proxessed as a dictionary
{
 "a_rate": 350,
 "cost": 100000,
 "critical_failure_mean": 0.0475,
 "demating": 392,
 "depth": 0.58,
 "height": 0.145,
 "id": 71,
 "mass": 35,
 "materials": "{\"low_alloyed_steel\": 28, \"copper\": 5.25, \"polyethylene\": 1.75}",
 "mating": 750,
 "n": 3,
 "non_critical_failure_mean": 0,
 "v_rate": 3300,
 "width": 0.58
}
```

### Direct backends communication example : Catalog BE to Main Moduse BE request

The test code `get_users_from_mm.py` is the test example of request/response from Catalog BE to Main Module BE to get users.

```shell
cd <${BACKEND}>

python core/get_users_from_mm.py
```

It can be executed from running container :

```shell
docker exec cm-backend python core/get_users_from_mm.py
```
